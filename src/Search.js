import React, { Component } from "react";

export default class Search extends Component {
   constructor(){
       super()
       this.state =({
           cityName : "",
           zipCode:""
       }
       )
   }

   getCityData=()=>{
       let city =this.state.cityName
    //console.log(this.state.cityName)
    
    this.props.getData(city)
    // console.log(mainFunction)
   }


  render() {
    return (
      <div className="input-group mb-3">
        <input
          type="text"
          className="form-control"
          placeholder="Enter city name"
          aria-label="Recipient's username"
          aria-describedby="basic-addon2"
        //   onChange={(evt) => { console.log(evt.target.value); }}
         onChange ={(evt)=>this.setState({ cityName : evt.target.value },()=>console.log(this.state.cityName,"cityName"))}
        />
        <div className="input-group-append">
          <button className="btn btn-outline-secondary" type="button"
        // onClick={(evt) =>{ console.log(evt.target.value)}}
        onClick ={this.getCityData}
        >
            Search
          </button>
        </div>
      </div>
    );
  }
}
