import React from 'react'
import DayCards from "./DayCards"
import Search from './Search';


class WeekContainer extends React.Component {
    constructor () {
        super();
        this.state = {
            fullData: [],
            dailyData:[],
            cityData:""
        };
      }
   
    // componentDidMount = () => {
       
    //     const weatherURL =
    //     //`https://api.openweathermap.org/data/2.5/weather?q=bangalore&units=IN&appid=5275cc27af09ab8089b38a5799063d49`
    //     //  `http://api.openweathermap.org/data/2.5/forecast?zip=560002,IN&appid=5275cc27af09ab8089b38a5799063d49`
    //     `https://api.openweathermap.org/data/2.5/forecast?q=bangalore&units=metric&appid=5275cc27af09ab8089b38a5799063d49`
    //     fetch(weatherURL)
    //     .then(res => res.json())
    //     .then(data => {
    //     const dailyData = data.list.filter(reading => reading.dt_txt.includes("18:00:00"))

    //     this.setState({
    //     fullData: data.list,
    //     dailyData: dailyData
    //   }, () => console.log(this.state))
    // })
    //   }


  
   getData = (city) => {
       
      const weatherURL =
      `https://api.openweathermap.org/data/2.5/forecast?q=${city}&units=metric&appid=5275cc27af09ab8089b38a5799063d49`
     // `http://api.openweathermap.org/data/2.5/forecast?zip=574142,IN&appid=5275cc27af09ab8089b38a5799063d49`
      //`https://api.openweathermap.org/data/2.5/weather?q=bangalore&units=IN&appid=5275cc27af09ab8089b38a5799063d49`
      //  `http://api.openweathermap.org/data/2.5/forecast?zip=560002,IN&appid=5275cc27af09ab8089b38a5799063d49`
      
      console.log(weatherURL,"url")
      fetch(weatherURL)
      .then(res => res.json())
      .then(data => {
        console.log(data.list,"data")
      const dailyData = data.list.filter(reading => reading.dt_txt.includes("18:00:00"))

      this.setState({
      fullData: data.list,
      dailyData: dailyData,
      cityData:data.city.name
    }, () => console.log(this.state))
  })
    }




    // get daily data and pass it to the cards 
    
    formatDayCards = () => {
        return this.state.dailyData.map((reading, index) => 
                                        //console.log(reading,"reading",index,"index")
                                        <DayCards reading={reading} key={index}  />
                                        )
      }

    render() {
    
        return (
            <div className="container">
                <h1 className="display-2 jumbotron">5-Day Forecast</h1>
                <h1><Search getData={this.getData}/></h1>
                <h5 className="display-4 text-white" >{this.state.cityData}</h5> 
                <div className="row justify-content-center">

                   {this.formatDayCards()}

                </div>
            </div>

        )
    }
}

export default WeekContainer