import React from 'react'
import { FaArrowUp } from 'react-icons/fa'
import { FaArrowDown } from 'react-icons/fa'
const moment = require('moment');

const DayCards = ({ reading }) => {
    
    console.log(reading,"reading")
    let newDate = new Date()
    console.log(newDate,'newDate');
    const weekday = reading.dt * 1000
    console.log(weekday,"weekday")
    newDate.setTime(weekday)
    console.log(newDate,"After Set Time")
  
    const imgURL = `owf owf-${reading.weather[0].id} owf-5x`
  
    return (
      <div className="col-sm-2">
        <div className="card text-center">
          <h3 className="card-title">{moment(newDate).format('dddd')}</h3>
          <p className="text-muted">{moment(newDate).format('MMMM Do')}</p>
          <i className={imgURL}></i>
          <h2>{Math.round((reading.main.temp))}°C</h2>
          <div className="text-muted">
          <div className="row justify-content-around">
          <i><FaArrowUp />{Math.round((reading.main.temp_max))}°C </i>
          <i><FaArrowDown />{Math.round((reading.main.temp_min))}°C</i>
          </div>    
        
          </div>
          
          <div className="card-body">
            <p className="card-text">{reading.weather[0].description}</p>
          </div>
        </div>
      </div>
    )
  }
export default DayCards